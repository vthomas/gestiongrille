# 1. Objectif

Cette bibliothèque permet de faire de manière très simple des affichages d'images dans une grille régulière.

Il est aussi possible de prendre la main pour utiliser un objet `Graphics` et effectuer les opérations graphiques de son choix.

# 2. Compilation et lancement des démonstrations

Compilation des fichiers main stockés dans src vers un répertoire bin.
```
javac -cp src -d bin src/*.java
```

Execution de la première demonstration (affichage sans interaction).
```
java -cp bin;. MainBase.java
```

Exécution de la seconde démonstration (affichage et interaction avec attente)
```
java -cp bin;. MainAttente.java
```

Exécution de la seconde démonstration (interaction MVC pas d'attente bloquante)
```
java -cp bin;. MainMVC.java
```

## Remarques

1. attention au passage de classPath via l'option `-cp`
	- sous Windows le séparateur est ;
    - sous Linux le séparateur est :
2. un fichier Makefile est fourni dans le dépôt qui permet de compile les fichiers et de lancer les classes Main


# 3. Utilisation simple de la grille

La classe `AffichageGrille` fonctionne de manière simple à partir de quelques méthodes:

- constructeur `AffichageGrille(int nCasesX, int nCasesY)` qui prend en entrée un nombre de cases en X et en Y
- méthode `void ajouterSprite(int x, int y, String sprite)` qui permet d'ajouter un sprite en coordonnées (x,y) de la grille
- méthode `void supprimerSprite(int x, int y)` qui permet de supprimer tous les sprites à la case (x,y) de la grille
- méthode `void afficher()` qui crée une fenêtre et affiche le contenu de la grille. La fenetre se met à jour en fonction des ajouts / suppression ultérieurs.

La classe `AffichageGrille` utilise un gestionnaire de sprites `GestionSprites`. Celui-ci va chercher les images dans un répertoire qu'il est possible de modifier via la méthode `setRepertoire` de son gestionnaire de sprite.

La classe `MainBase` présente une utilisation simple de ces méthodes en affichant simplement une grille.

# 4. Gestion des interactions utilisateurs

Il y a deux manières d'utiliser la bibliothèque:

  - soit avec des appels bloquants : on attend une action utilisateur avant d'agir ;
  - soit avec un architecture MVC : les données réagissent aux actions utilisateur via des contrôleurs et la vue se met alors à jour.

## 4.1 Appels bloquants

### 4.1.1 Présentation générale

La classe `AffichageGrilleAttente` propose de méthodes bloquantes mettant l'application en attente tant que l'utilisateur n'a pas effectué une des opérations attendues.
Ces opérations peuvent être un clic souris ou l'appui d'une touche au clavier

La classe  `AffichageGrilleAttente` s'utilise à travers les méthodes suivantes:

- méthode `void afficher()` qui crée une fenêtre et affiche le contenu de la grille. La fenêtre possède les `listeners` permettant de répondre aux interactions utilisateurs
- méthode `Position attendreClic()` qui attend un clic de l'utilisateur et retourne la position de la case cliquée.
- méthode `KeyEvent attendreTouche()` qui attend l'appui d'une touche de la part de l'utilisateur et retourne un objet de type `KeyEvent` décrivant l'évènement.


La classe `MainAttente` présente une utilisation de ces classes

- elle construit une grille attente et affiche un labyrinthe
- elle entre dans une boucle demandant à l'utilisateur de cliquer sur une case pour ajouter des murs (ou sur stop pour sortir)
- elle demande d'appuyer sur une touche et s'arrête quand on appuie sur espace.
Chacune des méthodes utilisées est bloquante.

### 4.1.2 Diagramme de classe
![diagramme de classe](plantuml/Attente.png)

### 4.1.3 Diagramme de séquence
![diagramme de sequence](plantuml/Seq_MainAttente.png)

## 4.2 Approche MVC

- Pas d'appel bloquant
- mise à jour en fonction du modèle

### 4.2.1 Présentation générale


Le package `grilleMVC` fournit les classes basées sur Grille pour effectuer une approche MVC. Un exemple d'une telle utilisation est donné dans le package `grilleMVCLaby`.

- la classe `Laby` représente le modèle.
	- Elle contient le labyrinthe et la position du robot et propose différentes méthodes publiques pour modifier les données.
	- Elle hérite de `Sujet` et contient une liste d'observateurs
	- Lorsque les données sont mises à jour à travers une méthode publique, cette méthode appelle `notifyObservers()` pour demander aux méthodes de se mettre à jour
- la classe `VueGrilleRobot` est une vue de Laby
	- Elle hérite de la classe abstraite `ObserverGrille`
	- La méthode `update` est la méthode de `Observer` et doit être redéfini en fonction du problème. Ici, elle dessine le labyrinthe dans la grille en fonction du modèle.
	- Cette méthode est appelée par le modèle à chaque mise à jour
- la classe `ControleurClavier` est un contrôleur permettant de modifier le modèle en fonction de évènements clavier.
	- Cette classe implémente l'interface `KeyListener` destinée à réagir aux évènements clavier.
	- Lorsqu'on presse une touche, on appelle une méthode publique du modèle (ici déplacement du robot) qui demandera à son tour aux vues de se mettre à jour
-  la classe `ControleurSouris` est un contrôleur destiné à réagir aux évènements souris
	- Cette classe implémente l'interface `MouseListener` destinée à réagir aux évènements souris
	- Lorsqu'on clic sur une case, le contrôleur demande le changement de statut du mur au modèle (qui demande à son tour aux vues de se mettre à jour)


La classe `MainMVC` se charge d'utiliser toutes ces classes pour mettre en oeuvre le patron d'architecture MVC

- elle construit le modèle `Laby`
- elle construit la vue `VueGrilleRobot` et l'ajoute en tant qu'observateur du modèle
- elle construit les contrôleurs
- elle ajoute l'ensemble dans une fenêtre `JFrame` et l'affiche à l'écran


### 4.2.2 Diagramme de classe

![diagramme de classe MVC](plantuml/MVC.png)

### 4.2.3 Diagrammes de séquence

Diagramme de séquence en réaction aux évènements clavier.

![diagramme de séquence clavier](plantuml/Seq_MainMVC.png)

Diagramme de séquence en réaction aux événements souris.

![diagramme de séquence clavier](plantuml/Seq_MainMVCSouris.png)


# 5. Diagramme Complet

![diagramme de classe complet](plantuml/All.png)
