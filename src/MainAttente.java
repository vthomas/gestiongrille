import grilleAttente.AffichageGrilleAttente;
import grille.Position;

import java.awt.event.*;


/**
 * permet d'afficher une grille avec interaction
 * Cela utilise la classe grilleMVC.Laby qui possede une methode permettant d'afficher dans une grille donnée
 * <p>
 * L'interaction se fait a travers une mise en attente
 * - soit à la souris
 * - soit au clavier
 * <p>
 * TODO LEs deux en meme temps ? (attendre l'un ou l'autre ?)
 */
class MainAttente {

    public static void main(String[] args) throws Exception {

        // creation des données du labyrinthe
        int[][] laby = {
                {1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 2, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 3},
        };

        // creation de la grille
        int nbCasesX = laby.length;
        int nbCasesY = laby[0].length;
        AffichageGrilleAttente grille = new AffichageGrilleAttente(nbCasesX, nbCasesY);

        // remplissage de la grille avec des sprites
        for (int i = 0; i < nbCasesX; i++) {
            for (int j = 0; j < nbCasesY; j++) {

                // si c'est un 1 => met une brique
                if (laby[i][j] == 1)
                    grille.ajouterSprite(i, j, "brick-wall");

                // si c'est un 2 => met un robot
                if (laby[i][j] == 2)
                    grille.ajouterSprite(i, j, "vintage-robot");

                // si c'est un 3 => met un stop
                if (laby[i][j] == 3)
                    grille.ajouterSprite(i, j, "halt");
            }
        }

        // affiche la grille
        grille.afficher();

        // ########################################
        // interaction souris
        // ########################################

        System.out.println("cliquer pour ajouter des murs");
        System.out.println("cliquer Stop => arrete cette partie");

        // boucle tant que pas cliqué sur STOP
        boolean fini = false;
        while (!fini) {
            // attend un clic
            Position p = grille.attendreClic();
            System.out.println("- clic sur grille en " + p.x + "," + p.y);

            // si c'est STOP
            if (laby[p.x][p.y] == 3) {
                // on s'arrete
                System.out.println("- STOP -> arret ");
                fini = true;
            } else {
                // ajoute un mur à la position et on conitnu
                grille.ajouterSprite(p.x, p.y, "brick-wall");
                System.out.println("- attend prochain clic (STOP => arret)");
            }
        }

        // ########################################
        // interaction Clavier
        // ########################################

        fini = false;
        // repeter tant que pas espace
        while (!fini){
            System.out.println("attend touche => ESPACE pour finir");
            KeyEvent res = grille.attendreTouche();
            System.out.println("- touche "+res.getKeyCode());

            // si c'est espace
            if (res.getKeyCode() == KeyEvent.VK_SPACE)
                    fini=true;
        }

        // arret de l'application
        System.exit(0);
    }

}
