package grilleMVC;

import grille.AffichageGrille;


/**
 * un observateur qui utilise une grille
 * La methode update est a redéfinir
 * => soit on modifie la grille
 * => soit on genere une nouvelle grille
 */

public abstract class ObserverGrille extends AffichageGrille implements Observer {

    /**
     * nombre de cases de la grille
     */
    int nbCasesX, nbCasesY;

    /**
     * observateur avec une grille
     * @param nbX nombre de cases de la grille
     * @param nbY nombre de cases de la grille
     */
    public ObserverGrille(int nbX, int nbY){
        super(nbX,nbY);
    }
}
