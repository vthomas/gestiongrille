package grilleMVC;

/**
 * un observateur defini simplement par un update
 */
public interface Observer {
    void update();
}
