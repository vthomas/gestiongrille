package grilleMVC;

import java.util.*;

/**
 * classe qui gere les modeles (liste des vues et ajout / suppression des vues)
 * utilise le patron observer / observe
 */
public abstract class Sujet {
    /**
     * la liste des vues
     */
    List<Observer> observers;

    /**
     * gere la liste des vues (vide)
     */
    public Sujet() {
        // creation de la liste des vues
        this.observers = new ArrayList<>();
    }

    /**
     * ajoute une vue dans la liste des vues connues
     *
     * @param vue vue a ajouter
     */
    public void addObs(Observer vue) {
        // ajoute la vue dans la liste
        this.observers.add(vue);
    }

    /**
     * retire une vue
     */
    public void removeObs(Observer vue) {
        // retire la vue de la liste
        this.observers.remove(vue);
    }

    /**
     * demande la mise a jour de l'ensemble des vues connues
     */
    public void notifyObservers() {
        // pour chaque vue
        for (Observer obs : this.observers)
            // appeler mise a jour
            obs.update();
    }
}
