import grilleMVCLabyZombie.ControleurClavier;
import grilleMVCLabyZombie.ControleurSouris;
import grilleMVCLabyZombie.Laby;
import grilleMVCLabyZombie.VueGrilleRobot;

import javax.swing.*;
import java.util.Random;

public class MainMVCZombie {

    public static void main(String[] args) {
        // creation du modele labyrinthe
        Laby laby = new Laby();

        // creation de la vue
        VueGrilleRobot vue = new VueGrilleRobot(laby);
        laby.addObs(vue);

        // met ensemble dans une frame
        JFrame frame = new JFrame();

        // ajoute le controleur et la  grille de la vue
        frame.getContentPane().add(vue);
        frame.requestFocus();

        // ajoute controleur pour pouvoir agir en meme temps
        vue.addMouseListener(new ControleurSouris(laby));
        frame.addKeyListener(new ControleurClavier(laby));

        // forcer premiere mise a jour
        laby.notifyObservers();

        // lancement de la frame
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        // lancement de l'animation
        Random rand = new Random();
        while(true){
            int x = rand.nextInt(3)-1;
            int y = rand.nextInt(3)-1;
            //System.out.println(x + "," +y);
            laby.deplacerPJ(new int[]{x,y});

            try {
                Thread.sleep(500);
            }
            catch(Exception e){
                throw new Error("probleme thread.sleep");
            }
        }
    }

}
