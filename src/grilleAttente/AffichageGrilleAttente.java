package grilleAttente;

import grille.AffichageGrille;
import grille.Position;

import javax.swing.*;
import java.awt.event.*;

/**
 * permet d'avoir une classe avec des methodes bloquantes qui attendent une action de l'utilisateur
 * <li> soit  un click de souris (méthode  waitForPosition)</li>
 * <li> soit  une touche appuyeé (méthode waitForKey) </li>
 * <p>
 * La grille est affichee dans une frame cree automatiquement
 */
public class AffichageGrilleAttente extends AffichageGrille implements MouseListener, KeyListener {

    /**
     * frame d'affichage dans laquelle mettre la grille
     */
    JFrame f;

    /**
     * derniere position cliquee (en terme d'indice de grille)
     */
    Position positionCourante = null;

    /**
     * derniere touche appuyee
     */
    KeyEvent dernierKey = null;


    /**
     * constructeur d'une grille vide par defaut
     */
    public AffichageGrilleAttente(int tx, int ty) {
        super(tx, ty);
    }


    /**
     * permet de creer la fenetre et l'affiche à l'ecran
     */
    public void afficher() {
        f = new JFrame();
        f.setContentPane(this);
        f.pack();
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // ajoute les listener sur la frame
        this.addMouseListener(this);
        this.addKeyListener(this);
        this.requestFocus();
    }

    //###############################
    // partie gestion de la souris
    // a travers implémentation du MouseListener
    //###############################

    // ajoute mouselistener

    /**
     * retourne la position du dernier clic en mémoire et réinitialise a null
     *
     * @return position cliquée (en terme d'indices de case)
     */
    public Position getPosition() {
        Position positionDernierClic = null;
        // recupere la position et la réinitialise
        if (this.positionCourante != null) {
            positionDernierClic = this.positionCourante;
            this.init();
        }
        // retourne le dernierclic
        return positionDernierClic;
    }

    /**
     * met en attente le programme jusqu'à un clic utilisateur et retourne le clic
     *
     * @return la position dans la grille du clic qui vient d'etre fait
     * @throws Exception en cas de probleme dans Thread.sleep
     */
    public Position attendreClic() throws Exception {
        Position positionCliquee = getPosition();
        // tant qu'il n'y a pas eu de clic, on attend et redemande
        while (positionCliquee == null) {
            Thread.sleep(10);
            positionCliquee = getPosition();
        }
        // retourne le clic effectue
        return positionCliquee;
    }

    //###############################
    // partie mouselistener
    //###############################

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    /**
     * on detecte un clic au moment ou le bouton de la souris est presse
     *
     * @param e clic de souris
     */
    public void mousePressed(MouseEvent e) {
        // transforme les coord de la souris en indices d'une case
        int indiceCaseX = e.getX() / TAILLE_CASE;
        int indiceCaseY = e.getY() / TAILLE_CASE;
        // met a jour la case courante cliquée
        this.positionCourante = new Position(indiceCaseX, indiceCaseY);
    }

    public void mouseReleased(MouseEvent e) {
    }


    //###############################
    // partie gestion du clavier
    //###############################

    /**
     * se met en attente jusqu'à ce qu'une touche soit appuyée
     *
     * @return evenement de la touche appuyee
     * @throws Exception si un problème dans Thread.sleep
     */
    public KeyEvent attendreTouche() throws Exception {
        KeyEvent toucheAppuyee = getDernierKey();
        // on attent 10 ms et on redemande tant qu'il n'y a aucune touche appuyee
        while (toucheAppuyee == null) {
            Thread.sleep(10);
            toucheAppuyee = getDernierKey();
        }
        // retourne la touche appuyee
        return toucheAppuyee;
    }

    /**
     * retourne le dernier evenement clavier qui a été stocke depuis le dernier appel et reinitialise la mémoire
     *
     * @return deriner evenement clavier detecte
     */
    public KeyEvent getDernierKey() {
        // si un evenement a été detecte par le listener
        if (this.dernierKey != null) {
            // le stocke, reinitialise la memoire et le retourne
            KeyEvent res = this.dernierKey;
            this.init();
            return res;
        }
        // si pas d'evenement depuis dernier appel retourne null
        return null;
    }

    public void keyTyped(KeyEvent e) {
    }

    /**
     * stockage de l'evenement clavier detecte
     * @param e l'evenement a traiter
     */
    public void keyPressed(KeyEvent e) {
        // des qu'un evenement clavier se produit, on le stocke en attribut
        this.dernierKey = e;
    }

    public void keyReleased(KeyEvent e) {
    }


    /**
     * reinitialise les derniers evenements clavier et souris observés
     */
    public void init() {
        // reinitialise tout
        this.dernierKey = null;
        this.positionCourante = null;
    }

}
