package grille;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;


/**
 * un gestionnaire de sprites sous la forme d'un singleton
 * - un seul gestionnaire de sprites pour toute l'application
 * <p>
 * lorsqu'on cherche a afficher un sprite (via la méthode dessinerSprite)
 * - soit il a déja été charge et on l'affiche juste
 * - soit il n'a pas été chargé, on le charge, on le stocke et on l'affiche
 *
 * @author vincent.thomas
 */
public class GestionSprites {

    /**
     * permet de faire singleton
     * reference unique vers un seul gestionnaire de sprite
     */
    private static GestionSprites gestionSprite = null;

    /**
     * une image par defaut en cas de probleme de lecture
     */
    private static Image DEFAULT = null;

    /**
     * map qui permet de stocker les sprites déjà chargés
     */
    Map<String, Image> sprites;

    /**
     * le repertoire qui contient les sprites
     */
    String repertoire;

    /**
     * construit un gestionnaire de sprite vide
     */
    private GestionSprites() {
        // creation de la hashmap pour stocker les sprites
        this.sprites = new HashMap<>();
    }

    /**
     * patron singleton, permet de retourner le seul gestionnaire de sprite à travers l'attribut privé
     * - soit l'attribut privé existe et on le retourne
     * - soit il n'existe pas, on le crée, on le stocke et on le retourne	 *
     *
     * @return l'objet singleton qui gere les sprites
     */
    public static GestionSprites getSprites() {
        // s'il n'existe pas
        if (gestionSprite == null)
            // on le cree et le stocke
            gestionSprite = new GestionSprites();

        // on le retourne
        return gestionSprite;
    }

    // #########################################################################
    // METHODES DE GESTION DES SPRITES (ajout / chargement / stockage)
    // #########################################################################

    /**
     * methode qui permet d'ajouter le sprite passé en paramètre à la liste des sprites connus par le gestionnaire
     *
     * @param nomFichier le nom du fichier du sprite a ajouter
     */
    private void ajouterElement(String nomFichier) {
        try {
            // charge l'image du sprite
            Image image = ImageIO.read(new FileInputStream(nomFichier));

            // construit un nomSimple en retirant l'extension et le .
            String nomPur = nomFichier.substring(this.repertoire.length() + 1);
            String sansExtension = nomPur.split("\\.")[0];

            // si image est deja présente
            if (this.sprites.containsKey(sansExtension)) {
                // envoyer une erreur, on ne devrait jamais etre la
                throw new Error("probleme image deja chargee " + sansExtension);
            }

            // c'est un sprite non connu => ajoute le sprite au gestionnaire
            this.sprites.put(sansExtension, image);

        } catch (IOException e) {
            // en cas de soucis de chargement
            System.out.println("probleme de chargement avec image " + nomFichier);
            // retournera l'image par defaut
        }
    }

    /**
     * permet d'acceder à un sprite
     * - s'il est déja chargé, on le retourne
     * - sinon, on le charge, on le stocke et on le retourne
     *
     * @param nom le nom du sprite
     * @return l'image correspondant au sprite
     */
    private Image chargerSprite(String nom) {

        // on regarde si le sprite existe, si c'est le cas => on le retourne
        if (this.sprites.containsKey(nom)) {
            // s'il existe, on le recupere
            return (this.sprites.get(nom));
        }

        // si le repertoire de chargement est null, on retourne une image par defaut
        if (this.repertoire == null) {
            // le repertoire est null
            System.out.println("Sprite manquant, pas de repertoire > " + nom);
            return (imageParDefaut());
        }

        // => le sprite n'est pas connu mais on a le repertoire
        // on tente de le charger si le repertoire n'est pas null
        this.ajouterElement(this.repertoire + "/" + nom + ".png");

        // si le sprite a bien ete ajoute, on le retourne
        if (this.sprites.containsKey(nom)) {
            return (this.sprites.get(nom));
        }

        // le sprite n'a pas été ajouté
        // c'est que l'image n'est pas dans le repertoire
        System.out.println("Sprite manquant dans le repertoire >" + nom);
        // on retourne une image par defaut
        return (imageParDefaut());
    }


    /**
     * modifie le repertoire par defaut où aller chercher les sprites
     *
     * @param rep le repertoire par defaut où les sprites sont stockés
     */
    public void setRepertoire(String rep) {
        this.repertoire = rep;
    }

    // #########################################################################
    // METHODES DE DESSIN DES SPRITES
    // #########################################################################

    /**
     * permet de dessiner un sprite donne dans la grille en passant un graphics
     *
     * @param nom    nom du sprite a dessiner
     * @param x      abscisse dans la grille
     * @param y      ordonnee dans la grille
     * @param taille taille du sprite à dessiner
     * @param g      graphics dans lequel dessiner
     */
    public void dessinerSprite(String nom, int x, int y, int taille, Graphics g) {
        Image img = null;
        img = chargerSprite(nom);
        g.drawImage(img, x, y, taille, taille, null);
    }

    /**
     * permet de dessiner un sprite donne avec deux tailles tailleX et tailleY
     *
     * @param nom     nom du sprite
     * @param x       abscisse dans la grille
     * @param y       ordonnee dans la grille
     * @param tailleX taille du sprite selon x
     * @param tailleY taille du sprite selon y
     * @param g       graphics dans lequel dessiner
     */
    public void dessinerSprite(String nom, int x, int y, int tailleX, int tailleY, Graphics g) {
        Image img = null;
        img = chargerSprite(nom);
        g.drawImage(img, x, y, tailleX, tailleY, null);
    }


    /**
     * permet de dessiner un sprite noir et blanc en couleur
     * - remplace noir par la couleur donnee
     * - stocke le sprite transforme dans la liste
     *
     * @param nom    nom du sprite
     * @param x      position dans la grille
     * @param y      position dans la grille
     * @param taille taille du sprite
     * @param c      couleur qui remplace le noir
     * @param g      graphics pour dessiner l'image
     */
    public void dessinerSpriteCouleur(String nom, int x, int y, int taille, Graphics g, Color c) {
        // construit le nom avec la couleur
        String nomCouleur = nom + "_" + c;

        // si l'image coloree n'existe pas deja, on la cree
        if (!this.sprites.containsKey(nomCouleur)) {
            // recupere l'image et on en fait une copie
            BufferedImage imgNB = (BufferedImage) chargerSprite(nom);
            BufferedImage coul = new BufferedImage(imgNB.getWidth(), imgNB.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
            Graphics gCopie = coul.getGraphics();
            gCopie.drawImage(imgNB, 0, 0, imgNB.getWidth(), imgNB.getHeight(), null);
            gCopie.dispose();

            // on colorie l'image en remplacant le noir par la couleur
            int rgb = c.getRGB();
            for (int i = 0; i < imgNB.getWidth(null); i++)
                for (int j = 0; j < imgNB.getHeight(null); j++) {
                    int col = imgNB.getRGB(i, j);
                    int red = (col >> 16) & 0xff;
                    int green = (col >> 8) & 0xff;
                    int blue = (col >> 0) & 0xff;
                    int alpha = (col >> 24) & 0xff;
                    // si c'est noir et non transparent, on colorie
                    if (red == 255 && green == 255 && blue == 255 && alpha == 255) {
                        coul.setRGB(i, j, rgb);
                    }
                }

            // on sauve l'image
            this.sprites.put(nomCouleur, coul);
        }

        // on l'affiche
        Image img = this.sprites.get(nomCouleur);
        g.drawImage(img, x, y, taille, taille, null);
    }



    // ########################################################################
    // GESTION DE L'IMAGE PAR DEFAUT (en cas d'image absente)
    // ########################################################################

    /**
     * @return image par defaut en cas d'erreur
     */
    static Image imageParDefaut() {
        if (DEFAULT == null)
            creerImageDefault();
        return DEFAULT;
    }

    /**
     * creer une image par defaut
     */
    private static void creerImageDefault() {
        System.out.println("creation image default");
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g = (Graphics2D) image.getGraphics();
        g.setColor(Color.red);
        g.setStroke(new BasicStroke(10));
        g.drawLine(0, 100, 100, 0);
        g.drawLine(0, 0, 100, 100);
        g.drawOval(25, 25, 50, 50);

        g.dispose();
        DEFAULT = image;
    }

}
