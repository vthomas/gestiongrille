package grille;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * classe qui permet de gerer un PAnel pour affichage
 * la classe est utilisée dans MVC ou dans GrilleAttente (appels bloquants)
 */
public class AffichageGrille extends JPanel {


    /**
     * taille par défaut des cases
     */
    public static int TAILLE_CASE = 50;


    /**
     * l'ensemble des objets a afficher
     * sous la forme d'une MAP : Position -> La liste des sprites
     */
    Map<Position, List<String>> objets;


    /**
     * la taille de la fenetre (précisee à la construction)
     * correspond au nombre de cases
     */
    int nbCasesX, nbCasesY;

    /**
     * construit une grille vide
     *
     * @param nbCasesX nombre de cases selon X
     * @param nbCasesY nombre de cases selon Y
     */
    public AffichageGrille(int nbCasesX, int nbCasesY) {
        // sauvegarde des attributs
        this.nbCasesX = nbCasesX;
        this.nbCasesY = nbCasesY;

        // creation d'une taille de fenetre préferentielle
        // (quand on insere dans une frame)
        Dimension preferredSize = new Dimension(nbCasesX * TAILLE_CASE, nbCasesY * TAILLE_CASE);
        this.setPreferredSize(preferredSize);

        // creation d'un ensemble d'objets vide
        this.objets = new HashMap<Position, List<String>>();
    }


    // ##################################################################################
    // AFFICHAGE DE LA GRILLE
    // ##################################################################################


    /**
     * redéfinition paint pour l'affichage
     * cette methode est appelee a chaque fois que la fenetre doit être reaffichee
     */
    public void paint(Graphics g) {
        // mise a jour de l'affichage
        super.paint(g);

        // recuperation du gestionnaire de sprites unique (patron singleton)
        GestionSprites sprites = GestionSprites.getSprites();
        sprites.setRepertoire("images");

        // dessine les cases de la grille une a une
        for (int x = 0; x < this.nbCasesX; x++) {
            for (int y = 0; y < this.nbCasesY; y++) {
                // contour de la case
                g.drawRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);

                // les sprites a dessiner en position x,y
                Position p = new Position(x, y);
                List<String> liste = this.objets.get(p);
                // s'il y en a, les dessiner
                if (liste != null) {
                    // pour chaque sprite a dessiner
                    for (String s : liste) {
                        sprites.dessinerSprite(s, x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, g);
                    }
                }
            } // fin boucle colonne
        } // fin boucle sur les cases
    }


    // ##################################################################################
    // GESTION DES ELEMENTS DE LA GRILLE
    // ##################################################################################

    /**
     * permet d'ajouter un sprite au bon endroit
     *
     * @param x      coordonne dimension X de la case
     * @param y      coordonne dimension Y de la case
     * @param sprite le nom du sprite a empiler sur cette case
     */
    public void ajouterSprite(int x, int y, String sprite) {
        // creation de la position pour la map
        Position p = new Position(x, y);

        // on cree la liste s'il n'y en a pas (premier sprite d'une position)
        if (!this.objets.containsKey(p)) {
            this.objets.put(p, new ArrayList<String>());
        }
        // on ajoute le sprite a la liste
        this.objets.get(p).add(sprite);

        // redessine
        repaint();
    }

    /**
     * permet de supprimer les sprites dans la case (x,y)
     *
     * @param x position case x
     * @param y position case y
     */
    public void supprimerSprite(int x, int y) {
        // supprime la liste en (x,y)
        Position position = new Position(x, y);
        this.objets.put(position, null);
    }


    /**
     * permet de supprimer tous les sprites de la grille
     */
    public void supprimerTout() {
        // on reinitialise la hashmap
        this.objets = new HashMap<>();

        // met a jour
        repaint();
    }
}
