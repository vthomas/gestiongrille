package grille;

/**
 * permet de representer une position sur la grille
 * defini par deux entier x et y
 * <p>
 * utilisee dans l'affichage et le stockage des sprites a afficher
 */
public class Position {

    // position selon x et y
    public int x, y;

    /**
     * constructeur par defaut
     *
     * @param i position en x
     * @param j position en y
     */
    public Position(int i, int j) {
        this.x = i;
        this.y = j;
    }

    /**
     * hashcode pour la hashmep => egalite si x et y egaux
     * @return hashcode
     */
    public int hashCode() {
        return x + 31 * y;
    }

    /**
     * egaite si x et y egaux
     * @param o autre objet a comparer (ici autre position)
     * @return vrai si les deux objets ont les memes coordonnées
     */
    public boolean equals(Object o) {
        // si c'ets pas une position => pas egalite
        if (!(o instanceof Position))
            return false;
        // sinon teste les coordonnées
        Position p = (Position) o;
        return (x == p.x) && (y == p.y);
    }
}