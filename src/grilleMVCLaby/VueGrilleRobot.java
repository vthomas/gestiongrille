package grilleMVCLaby;

import grille.AffichageGrille;
import grilleMVC.ObserverGrille;

/**
 * vue d'une grilleROBOT
 */
public class VueGrilleRobot extends ObserverGrille {

    // ################################################
    // CONSTANTE DE SPRITES
    // ################################################
    public static final String BRICK_WALL = "brick-wall";
    public static final String ROBOT = "vintage-robot";

    // ################################################
    // CODE DE LA VUE
    // ################################################

    /**
     * attribut modele
     */
    Laby modele;


    /**
     * observateur d'un labyrinth
     *
     * @param modele labyrinthe a suivre
     */
    public VueGrilleRobot(Laby modele) {
        // construit  a la taille du modele
        super(modele.getTailleX(), modele.getTailleY());

        // stocke le modele
        this.modele = modele;

    }

    /**
     * dessine le labyrinthe et le robot dans la grille
     */
    @Override
    public void update() {
        // on retire tout de la grille
        this.supprimerTout();

        // affiche les murs
        int[][] laby = this.modele.getLaby();
        for (int i = 0; i < laby.length; i++) {
            for (int j = 0; j < laby[0].length; j++) {
                if (laby[i][j] == 1)
                    this.ajouterSprite(i, j, BRICK_WALL);
            }
        }

        // affiche le robot
        this.ajouterSprite(this.modele.robotX, this.modele.robotY, ROBOT);

        // mise a jour grille
        this.repaint();
    }
}
