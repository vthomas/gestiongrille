import grille.AffichageGrille;
import grilleAttente.AffichageGrilleAttente;

/**
 * classe main de base pour tester AfficherGrilleAttente
 */
class MainBase {

    /**
     * permet juste d'afficher une grille sans interaction
     *
     * @param args inutile
     */
    public static void main(String[] args) {
        // on précise la taille des cases
        AffichageGrille.TAILLE_CASE = 100;

        // on construit une grille vide de 5x5
        AffichageGrilleAttente grille = new AffichageGrilleAttente(5, 5);

        // on ajoute des sprites (stockés dans images)
        grille.ajouterSprite(0, 0, "greek-sphinx");
        grille.ajouterSprite(1, 0, "greek-sphinx");
        grille.ajouterSprite(2, 0, "mammoth");

        // change ligne
        grille.ajouterSprite(3, 2, "mammoth");

        // sprite absent pour voir l'image par defaut
        grille.ajouterSprite(3, 0, "xxx");

        // suppression des sprites en 0,0
        grille.supprimerSprite(0,0);

        // on affiche la grille
        grille.afficher();

        // on ajoute apres avoir affiche la grille ... la grille se met a jour
        grille.ajouterSprite(2, 2, "greek-sphinx");

        // le programme s'arrete en cliquant sur la croix de l'application
    }

}