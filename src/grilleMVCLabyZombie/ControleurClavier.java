package grilleMVCLabyZombie;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ControleurClavier implements KeyListener {

    Laby modele;

    public ControleurClavier(Laby m) {
        this.modele = m;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // en fonction de la touche appuyee
        switch (e.getKeyCode()) {
            // gauche
            case KeyEvent.VK_LEFT:
                this.modele.deplacerRobot(new int[]{-1,0});
                break;

            // droite
            case KeyEvent.VK_RIGHT:
                this.modele.deplacerRobot(new int[]{1, 0});
                break;

            // bas
            case KeyEvent.VK_DOWN:
                this.modele.deplacerRobot(new int[]{0, 1});
                break;

            // haut
            case KeyEvent.VK_UP:
                this.modele.deplacerRobot(new int[]{0, -1});
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
