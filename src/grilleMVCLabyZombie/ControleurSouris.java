package grilleMVCLabyZombie;

import grille.AffichageGrille;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControleurSouris implements MouseListener {

    Laby modele;

    public ControleurSouris(Laby m) {
        this.modele = m;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // coordonnees case
        int x = e.getX() / AffichageGrille.TAILLE_CASE;
        int y = e.getY() / AffichageGrille.TAILLE_CASE;
        // permet de changer le mur
        this.modele.changerMur(x, y);

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
