package grilleMVCLabyZombie;


import grilleMVC.Sujet;

/**
 * Modele selon une approche MVC
 * <p>
 * le modele est constitue de
 * (a) la grille
 * (b) la position de l'agent unique
 * <p>
 * Le Labyrinthe extends Sujet (c'est le modele dans  MVC)
 */

public class Laby extends Sujet {


    // ################################################
    // ATTRIBUTS DU MODELE
    // ################################################

    /**
     * labyrinthe connu
     */
    int[][] laby;

    /**
     * attributs position du robot
     */
    int robotX, robotY;

    /**
     * position de la personne
     */
    int PJX, PJY;


    /**
     * creation du labyrinthe par défaut
     */
    public Laby() {
        // creation des données du labyrinthe
        int[][] laby = {
                {1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1},
        };
        this.laby = laby;

        // l'agent est en 3,3 (juste pour tester)
        this.robotX = 3;
        this.robotY = 3;

        // une personne se deplace au hasard
        this.PJX = 4;
        this.PJY = 4;
    }


    // ################################################
    // GESTION DES DEPLACEMENTS DU ROBOT
    // ################################################

    /**
     * cherche a deplacer le robot en fonction d'un deplacement donné
     *
     * @param dep deplacement du robot avec dep[0] variation en X et dep[1] variation en y
     */
    public void deplacerRobot(int[] dep) {
        // calcule des coordonnees
        int nX = this.robotX + dep[0];
        int nY = this.robotY + dep[1];

        // si la case est possible
        if (this.caseDisponible(nX, nY)) {
            // deplacer le robot
            this.robotX = nX;
            this.robotY = nY;
        }

        // demander la mise a jour des vues
        this.notifyObservers();
    }

    /**
     * cherche a deplacer le robot en fonction d'un deplacement donné
     *
     * @param dep deplacement du robot avec dep[0] variation en X et dep[1] variation en y
     */
    public void deplacerPJ(int[] dep) {
        // calcule des coordonnees
        int nX = this.PJX + dep[0];
        int nY = this.PJY + dep[1];

        // si la case est possible
        if (this.caseDisponible(nX, nY)) {
            // deplacer le robot
            this.PJX = nX;
            this.PJY = nY;
        }

        // demander la mise a jour des vues
        this.notifyObservers();
    }

    /**
     * permet de savoir si une case est disponible
     *
     * @param x position x
     * @param y position y
     * @return true si la case est disponible
     */
    public boolean caseDisponible(int x, int y) {
        // si on est hors labyrinthe => false
        if ((x < 0) || (x >= this.getTailleX()) || (y < 0) || (y >= this.getTailleY()))
            return false;

        // s'il y a un mur => false ou true sinon
        // retourne disponible = non(il y a un mur)
        boolean disponible = !(this.laby[x][y] == 1);
        return disponible;
    }

    /**
     * permet de changer le status du mur
     * @param x case x du mur
     * @param y case y du mur
     */
    public void changerMur(int x, int y) {
        // si on est hors labyrinthe => rien
        if ((x < 0) || (x >= this.getTailleX()) || (y < 0) || (y >= this.getTailleY()))
            return ;

        // changer la valeur du mur
        this.laby[x][y] = 1 - this.laby[x][y];

        // update
        notifyObservers();
    }


    // ################################################
    // GETTER
    // ################################################

    /**
     * @return taille selon x
     */
    public int getTailleX() {
        return this.laby.length;
    }

    /**
     * @return taille selon y
     */
    public int getTailleY() {
        return this.laby[0].length;
    }

    public int[][] getLaby() {
        return this.laby;
    }
}
