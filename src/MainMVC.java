import grilleMVCLaby.ControleurClavier;
import grilleMVCLaby.ControleurSouris;
import grilleMVCLaby.Laby;
import grilleMVCLaby.VueGrilleRobot;

import javax.swing.*;

/**
 * construit labyrinthe selon une approche MVC
 */
public class MainMVC {

    public static void main(String[] args) {
        // creation du modele labyrinthe
        Laby laby = new Laby();

        // creation de la vue
        VueGrilleRobot vue = new VueGrilleRobot(laby);
        laby.addObs(vue);

        // creation du controleur
        ControleurClavier clavier = new ControleurClavier(laby);
        ControleurSouris souris = new ControleurSouris(laby);

        // met ensemble dans une frame
        JFrame frame = new JFrame();

        // ajoute le controleur et la  grille de la vue
        frame.getContentPane().add(vue);
        frame.addKeyListener(clavier);
        vue.addMouseListener(souris);
        frame.requestFocus();

        // forcer premiere mise a jour
        laby.notifyObservers();

        // lancement de la frame
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
