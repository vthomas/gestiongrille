
#####################################
## COMPILATION JAVA
#####################################

all : mainMVC

prep :
	mkdir -p bin

mainBase : prep
	javac -cp src -d bin src/*.java
	java -cp bin:. MainBase

mainAttente : prep
	javac -cp src -d bin src/*.java
	java -cp bin:. MainAttente

mainMVC : prep
	javac -cp src -d bin src/*.java
	java -cp bin:. MainMVC

clean :
	rm -r bin

#####################################
## DOCUMENTATION
#####################################

generateDoc : generateDocHTML

generateDocHTML :
	pandoc README.md -o README.html

generateDocPdf :
	pandoc README.md -o README.pdf
